
package usecase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import model.Driver;

public class GameOperation {
    
    Driver dri;
    ArrayList<Driver> arrayDri = new ArrayList();
   
    public static int tirarDado(int rango) {
        double resultado;
        resultado=Math.random()*rango; 
        return (int)resultado + 1;
    }
    
    public static int avanzar(){
        int distancia = tirarDado(6)*100;
        return distancia;
    } 
    
    public int cantidadTiradasDadoParaFinalizarRecorrido(int lengthTrackKm){
        int contadorDeTiradas = 0;
        int sumaDeDistancias = 0;
        while(sumaDeDistancias < (lengthTrackKm*1000)){
            sumaDeDistancias += avanzar();
            contadorDeTiradas ++;
        }
        return contadorDeTiradas;
    }
    
    public ArrayList crearListaDriver(){
        String[] menuNamesDriver = {"Ezio", "Kratos", "Iron Man", "Spiderman", "Doctor Who", "Link", "Sonic", "Dante", "Goku", "Darth Vader"};     
        for(int i = 0; i < menuNamesDriver.length; i++){
//            int sisi = (int) (Math.floor(Math.random() * (75-25+1)) + 25);
            dri = new Driver(menuNamesDriver[i], 0);
            arrayDri.add(dri);
        }
        return arrayDri;
    }
    
    public void actualizarPuntuacionGanador(){
        
    }
    
    public void ordenar (){
        GameOperation obj = new GameOperation();
        arrayDri = obj.crearListaDriver();
        Collections.sort(arrayDri, new Comparator<Driver>(){
          
        @Override
	public int compare(Driver p1, Driver p2) {
		return new Integer(p1.getCantidadDeTiradasDado()).compareTo(new Integer(p2.getCantidadDeTiradasDado()));
	}
        
      });
   
      for( int i=0; i< arrayDri.size();i++){
          System.out.println(arrayDri.get(i).getNameDriver() + " \t" + arrayDri.get(i).getCantidadDeTiradasDado());
      }
        
    }
    
 }   
 
