
package views;

import javax.swing.JOptionPane;
import model.Game;

public class ViewsOne {
    private Game game;
    
    private final String menuInicio =   " -----------  BIENVENIDO AL JUEGO PISTA LOCA -----------\n\n" +
                        "        Selecciona un jugador del 1 al 10 para jugar \n" +
                        " 1.  Ezio \n" +
                        " 2.  Kratos \n" + 
                        " 3.  Iron Man \n" +
                        " 4.  Spiderman\n" +
                        " 5.  Doctor Who\n" +
                        " 6.  Link\n" +
                        " 7.  Sonic\n" + 
                        " 8.  Dante \n" +
                        " 9.  Goku \n" +
                        "10.  Darth Vader\n";
    
    private final String menuCar =  "        Selecciona vehículo del 1 al 10 para jugar \n" +
                        " 1.  FIA \n" +
                        " 2.  Hummer \n" + 
                        " 3.  Lamborgini \n" +
                        " 4.  Audi\n" +
                        " 5.  Kia\n" +
                        " 6.  Renault\n" +
                        " 7.  Ferrari\n" + 
                        " 8.  Dodge \n" +
                        " 9.  Ford \n" +
                        "10.  Jeep \n";
    
    private final String menuTrack =  "        Selecciona una pista del 1 al 10 para jugar \n" +
                        " 1.  Francia \n" +
                        " 2.  Emiratos \n" + 
                        " 3.  Marruecos \n" +
                        " 4.  Sudafrica\n" +
                        " 5.  Colombia\n" +
                        " 6.  Argentina\n" +
                        " 7.  Honduras\n" + 
                        " 8.  Japon \n" +
                        " 9.  China \n" +
                        "10.  Rusia \n";
    
    private final String menuLane =  "Selecciona un Carril de la pista del 1 al 10 para jugar \n" +
                            " 1.  Carril  1 \n" +
                            " 2.  Carril  2 \n" + 
                            " 3.  Carril  3 \n" +
                            " 4.  Carril  4 \n" +
                            " 5.  Carril  5 \n" +
                            " 6.  Carril  6 \n" +
                            " 7.  Carril  7 \n" + 
                            " 8.  Carril  8 \n" +
                            " 9.  Carril  9 \n" +
                            "10.  Carril 10 \n";
    
    private final String menuDistance =  "Selecciona una distancia para el recorrido de \n "
                                + "la pista del 1 al 6 para jugar \n" +
                            " 1.  2 Km  1 \n" +
                            " 2.  4 Km \n" + 
                            " 3.  5 Km \n" +
                            " 4.  7 Km \n";
    
    public int welcome (){
        int wel;             
        return wel = Integer.parseInt(JOptionPane.showInputDialog(menuInicio));
    }
    
    public int chooseCar(){
        int cr;        
        return cr = Integer.parseInt(JOptionPane.showInputDialog(menuCar));
    }
    
    public int chooseTrack(){
        int ct;
        return ct = Integer.parseInt(JOptionPane.showInputDialog(menuTrack));
    }
    
    public int chooseLane(){
        int rl;
        return rl = Integer.parseInt(JOptionPane.showInputDialog(menuLane));
    }
    
    public int chooseDistance(){
        int cd;
        int[] outputDistance = {2, 4, 5, 7};
        return cd = outputDistance[Integer.parseInt(JOptionPane.showInputDialog(menuDistance)) - 1] ;
    }
    
    public void inicioJuego (){
        JOptionPane.showMessageDialog(null, "Daremos inicio al JUEGO ....\n\n   Tira el dado");        
    }
    
    
}
