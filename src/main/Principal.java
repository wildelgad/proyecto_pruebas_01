package main;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Car;
import model.Driver;
import model.Game;
import model.Lane;
import model.Track;
import usecase.GameOperation;
import views.ViewsOne;

public class Principal {

    public static void main(String[] args) {
        int varios;
        ViewsOne vista = new ViewsOne();
        Driver dr = new Driver();
        Car car = new Car();
        Track track = new Track();
        Lane lane = new Lane();
        Game game;
        GameOperation go = new GameOperation();
        ArrayList<Driver> arrL;
//        Podium podium = new Podium();
        arrL = go.crearListaDriver();
        //Mensaje de Bienvenida y Asigna Jugador
        dr.receiverDriver(vista.welcome());

        //Asigna Carro
        car.receiverCar(vista.chooseCar());

        //Asigna Pista
        track.receiverTrack(vista.chooseTrack());

        //Asigna Carril
        lane.receiverLane(vista.chooseLane());

        //Asigna Distancia
        int dist = vista.chooseDistance();
        track.setLengthTrack(dist);
       
        game = new Game(car, track, arrL);
//        System.out.println("ressss: " + game.getAl().get(6).getNameDriver());
        
        //Inicia el juego
        vista.inicioJuego();
        
        for (int i = 0; i < arrL.size(); i++){
            varios = go.cantidadTiradasDadoParaFinalizarRecorrido(dist);
            arrL.get(i).setCantidadDeTiradasDado(varios);
        }
        
        for (int i = arrL.size()-3; i < arrL.size(); i++){
            System.out.println("el puesto es: " + arrL.get(i).getCantidadDeTiradasDado());
           JOptionPane.showMessageDialog(null, "El puesto es: " + arrL.get(i).getCantidadDeTiradasDado());
        } 
        
                     
    }
}

//////
//////////
