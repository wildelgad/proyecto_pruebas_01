
package model;

import javax.swing.JOptionPane;

public class Driver{
    private String[] menuNamesDriver = {"Ezio", "Kratos", "Iron Man", "Spiderman", "Doctor Who", "Link", "Sonic", "Dante", "Goku", "Darth Vader"};
    private String nameDriver;
    private int cantidadDeTiradasDado;
    
    public Driver(){
        this.nameDriver = "";
        this.cantidadDeTiradasDado = 0;
    }
    
    public Driver(String nameDriver, int cantidadDeTiradasDado) {
        this.nameDriver = nameDriver;
        this.cantidadDeTiradasDado = cantidadDeTiradasDado;
    }
    
    public void receiverDriver(int rd){
        JOptionPane.showMessageDialog(null, "Se asignó correctamente el nombre de " + menuNamesDriver[rd-1]);
    }

    public String getNameDriver() {
        return nameDriver;
    }

    public void setNameDriver(String nameDriver) {
        this.nameDriver = nameDriver;
    }

    public int getCantidadDeTiradasDado() {
        return cantidadDeTiradasDado;
    }

    public void setCantidadDeTiradasDado(int cantidadDeTiradasDado) {
        this.cantidadDeTiradasDado = cantidadDeTiradasDado;
    }
    
}
