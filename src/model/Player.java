/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public abstract class Player {
    private String namePlayer;

    public Player(String namePlayer) {
        this.namePlayer = namePlayer;
    }

    public abstract void miMetodo();
    
    public String getNamePlayer() {
        return namePlayer;
    }

    public void setNamePlayer(String namePlayer) {
        this.namePlayer = namePlayer;
    }
    
    
}
////////