
package model;

import java.util.ArrayList;
import usecase.GameOperation;

public class Game {
//    private Driver driver;
    private Car car;
    private Track track;  
    private ArrayList<Driver> arrayL; 

    public Game(Car car, Track track, ArrayList<Driver> arrayL) {
        this.car = car;
        this.track = track;
        this.arrayL = arrayL;
    }
    
    public void beginGame(){
        
    }
    
    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public ArrayList<Driver> getAl() {
        return arrayL;
    }

    public void setAl(ArrayList<Driver> al) {
        this.arrayL = al;
    }


    
    
    
    
    
    
    
}
