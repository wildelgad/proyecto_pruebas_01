
package model;

public class Podium {
    private Driver player;
//    ArrayList
    private int positionFirst;
    private int positionSecond;
    private int positionThird;

    public Podium(Driver player, int positionFirst, int positionSecond, int positionThird) {
        this.player = player;
        this.positionFirst = positionFirst;
        this.positionSecond = positionSecond;
        this.positionThird = positionThird;
    }

//    public Driver getPlayer() {
//        return player;
//    }
//
//    public void setPlayer(Driver player) {
//        this.player = player;
//    }

    public int getPositionFirst() {
        return positionFirst;
    }

    public void setPositionFirst(int positionFirst) {
        this.positionFirst = positionFirst;
    }

    public int getPositionSecond() {
        return positionSecond;
    }

    public void setPositionSecond(int positionSecond) {
        this.positionSecond = positionSecond;
    }

    public int getPositionThird() {
        return positionThird;
    }

    public void setPositionThird(int positionThird) {
        this.positionThird = positionThird;
    }
    
    
}
////////////